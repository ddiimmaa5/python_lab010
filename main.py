import random
from User import User


class Bank:
    def __init__(self, __balance):
        self.__balance = __balance

    def addCount(self, add):
        self.__balance += add
        print(f'Ви поклали на рахунок {add}, баланс {self.__balance}')

    def minusCount(self, minus):
        if self.__balance > minus:
            self.__balance -= minus
            print(f'Ви зняли з рахунку {minus}, баланс {self.__balance}')
        else:
            print('На рахунку не достатньо коштів')


class Coin:
    def __init__(self, __sideup=0):
        a = random.randint(1, 2)
        if a == 1:
            self.__sideup = 'heads'
        else:
            self.__sideup = 'tails'
        print(f'Сторона: {self.__sideup}')

    def toss(self, n):
        for i in range(n):
            if self.__sideup == 'heads':
                self.__sideup = 'tails'
            else:
                self.__sideup = 'heads'
        print(f'Кількість підкидань: {n}, результат: {self.__sideup}')


class Car:
    def __init__(self, marka, model, year, speed=0):
        self.marka = marka
        self.model = model
        self.year = year
        self.speed = speed

    def accelerate(self):
        self.speed += 5

    def brake(self):
        self.speed -= 5

    def get_speed(self):
        print(f'Швидкість автомобіля {self.speed}')


class Pets():
    def pets_info(self):
        sum = dog.number_pets + dog_pug.number_pets
        print(f'Кількість вихованців: {sum}')
        names = dog.name + '\t\t' + dog_pug.name
        print(f'Клічки собак: {names}')
        breeds = dog.breed + '\t' + dog_pug.breed
        print(f'Породи собак: {breeds}')
        ages = '\t\t' + dog.age + '\t\t' + dog_pug.age
        print(f'Вік собак: {ages}')
        natures = dog.nature + '\t' + dog_pug.nature
        print(f'Характер собак: {natures}')


class Dog(Pets):
    mammal = 'Dog'
    nature = 'good'
    breed = 'shepherd'
    number_pets = 0

    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.number_pets += 1

    def get_info(self):
        print(f'Name: {self.name}, age: {self.age}')

    def get_behavior(self):
        if self.nature == 'good':
            print(f'Собака має хороший характер і добре піддається дресеруванню')
        elif self.nature == 'bad':
            print(f'Собака має поганий характер і поганий піддається дресеруванню')
        else:
            print('Передбачити поведінку собаки неможливо')


class Dog_pug(Dog):
    mammal = 'Dog'
    nature = '*'
    breed = 'pug'


class Buffer:

    def __init__(self):
        self.numbers = []

    def add(self, a):
        for i in range(len(a)):
            self.numbers.append(a[i])

    def get_current_part(self):
        while len(self.numbers) >= 5:
            fin = 0
            for i in range(5):
                fin += self.numbers[0]
                self.numbers.pop(0)
            print(fin)


class MyError(Exception):
    def __init__(self, text):
        self.text = text


coding = zip(
    [1000,900,500,400,100,90,50,40,10,9,5,4,1],
    ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]
)
coding1 = zip(
    ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"],
    [1000,900,500,400,100,90,50,40,10,9,5,4,1]
)
class rome():
    def __init__(self, num):
        self.num = num

    def toRoman(self):
        if self.num <= 0 or self.num >= 4000 or int(self.num) != self.num:
            raise ValueError('Input should be an integer between 1 and 3999')
        result = []
        for d, r in coding:
            while self.num >= d:
                result.append(r)
                self.num -= d
        return ''.join(result)


class negRome(rome):
    def fromRoman(self):
        a = [*self.num]
        res = 0
        for d, r in coding1:
            for i in range(len(a)):
                if a[i] == d:
                    res += r
        return res


class Shop:
    def __init__(self, shop_name, store_type):
        self.shop_name = shop_name
        self.store_type = store_type

    def describe_shop(self):
        print(f'Name: {self.shop_name}, type: {self.store_type}')

    def open_shop(self):
        if self.store_type == 'open':
            print(f'Shop are open')
        else:
            print(f'Shop are closed')
    number_of_units = 0

    def set_number_of_units(self, n):
        self.number_of_units = n

    def increment_number_of_units(self, n):
        self.number_of_units += n


class Discount(Shop):
    dicount_products = ''

    def get_discount_products(self):
        print(f'Products with discount: {self.dicount_products}')


class Admin(User):
    pri = None


class Privileges:
    privileges = ['Allowed to add message', 'Allowed to delete users', 'Allowed to ban users']

    def show_privileges(self):
        print(f'Privileges: {self.privileges}')


if __name__ == "__main__":
    client1 = Bank(100)
    client1.addCount(50)
    client1.minusCount(50)
    client1.minusCount(150)

    coin = Coin()
    coin.toss(50)

    car = Car('Porshe', 'GT', 2012, speed=150)
    for i in range(5):
        car.accelerate()
        car.get_speed()
    print('-------------------------')
    for i in range(5):
        car.brake()
        car.get_speed()

    dog = Dog('Bobik', '2')
    dog.get_info()
    dog.get_behavior()
    dog_pug = Dog_pug('Gosha', '1')
    dog_pug.get_info()
    dog_pug.get_behavior()
    dog_pug.pets_info()

    num1 = Buffer()
    a = [int(i) for i in input('Введіть елементи для додавання: \n').split()]
    num1.add(a)
    num1.get_current_part()
    a = [int(i) for i in input('Введіть елементи для додавання: \n').split()]
    num1.add(a)
    num1.get_current_part()

    a = input('Ведіть ім\'я: ')
    try:
        if len(a) < 10:
            raise MyError('Ім\'я меньше ніж 10 символів')
    except ValueError:
        print("Error type of value!")
    except MyError as mr:
        print(mr)
    a = input('Ведіть ім\'я: ')
    try:
        if len(a) < 10:
            raise MyError('Ім\'я меньше ніж 10 символів')
    except ValueError:
        print("Error type of value!")
    except MyError as mr:
        print(mr)

    to = rome(55)
    frome = negRome('LV')
    print(to.toRoman())
    print(frome.fromRoman())

    store = Shop('Metro', 'open')
    store.describe_shop()
    store.open_shop()
    store1 = Shop('ATB', 'closed')
    store1.describe_shop()
    store2 = Shop('Mango', 'open')
    store2.describe_shop()
    store3 = Shop('ECO', 'open')
    store3.describe_shop()
    store = Shop('Metro', 'open')
    print(f'Units: {store.number_of_units}')
    store.number_of_units += 1
    print(f'Units: {store.number_of_units}')
    store.set_number_of_units(5)
    print(f'Units: {store.number_of_units}')
    store.increment_number_of_units(10)
    print(f'Units: {store.number_of_units}')
    store_discount = Discount('ECO', 'open')
    store_discount.get_discount_products()

    user = User('Richard', 'Enderson', 123, 'La[p]')
    user.describe_user()
    user.greeting_user()
    user1 = User('Vasia', 'Pupkin', 123, 'VaSA')
    user1.describe_user()
    user1.greeting_user()
    user = User('Richard', 'Enderson', 123, 'La[p]')
    user.increment_login_attempts()
    user.increment_login_attempts()
    user.increment_login_attempts()
    print(f'Login attempts: {user.login_attempts}')
    user.reset_login_attempts()
    print(f'Login attempts: {user.login_attempts}')
    admin = Admin('Odmen', 'Odmenovich', 321, 'oDlen')
    admin.pri = Privileges()
    admin.pri.show_privileges()