class User:
    login_attempts = 0

    def __init__(self, first_name, last_name, password, nickname):
        self.first_name = first_name
        self.last_name = last_name
        self.password = password
        self.nickname = nickname

    def describe_user(self):
        print(f'{self.first_name} {self.last_name}')

    def greeting_user(self):
        print(f'Wellcome, {self.first_name}')

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0


