from User import User


class Admin(User):
    pri = None


class Privileges:
    privileges = ['Allowed to add message', 'Allowed to delete users', 'Allowed to ban users']

    def show_privileges(self):
        print(f'Privileges: {self.privileges}')


user = User('Richard', 'Enderson', 123, 'La[p]')
user.describe_user()
user.greeting_user()
user1 = User('Vasia', 'Pupkin', 123, 'VaSA')
user1.describe_user()
user1.greeting_user()
user = User('Richard', 'Enderson', 123, 'La[p]')
user.increment_login_attempts()
user.increment_login_attempts()
user.increment_login_attempts()
print(f'Login attempts: {user.login_attempts}')
user.reset_login_attempts()
print(f'Login attempts: {user.login_attempts}')
admin = Admin('Odmen', 'Odmenovich', 321, 'oDlen')
admin.pri = Privileges()
admin.pri.show_privileges()